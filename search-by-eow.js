const extensionId = 'search-by-eow';
const baseURL = 'https://eow.alc.co.jp/search';

browser.contextMenus.create({
  id: extensionId,
  title: browser.i18n.getMessage('menuTitle'),
  contexts: ['selection'],
  icons: {
    "315": "icon-315.png"
  }
}, () => {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  }
});

browser.contextMenus.onClicked.addListener((info, tab) => {
  switch (info.menuItemId) {
  case extensionId:
    let text = info.selectionText;
    let url = new URL(baseURL);
    url.searchParams.set('q', text);
    browser.tabs.create({
      url: url.toString()
    })
      .catch(error => {
        console.error(error);
      });
    break;
  }
});
